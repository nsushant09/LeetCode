package linearSearch;

public class FindNumbersWithEvenNumberOfDigits {

    public static void main(String[] args) {
        int [] nums = {12, 345, 2, 6, 7896};
        System.out.println(findNumbers(nums));
    }
    static public int findNumbers(int[] nums) {
        int count = 0;
        for(int element : nums){
            if(even(element)){
                count++;
            }
        }
        return count;

    }
    static boolean even(int num){
        int count = 0;
        while(num > 0){
            count ++;
            num/=10;
        }
        return count % 2 == 0;
    }
}
