package binarysearch

fun main(){
    val nums = intArrayOf(1,3,5,6)
    val target = 2
    println(searchInsert(nums, target))
}
fun searchInsert(nums: IntArray, target: Int): Int {

    var first = 0
    var last = nums.size - 1
    var mid = -1;

    while(first <= last){
        mid = first + (last - first) / 2
        if(nums[mid] == target){
            return mid;
        }
        if(nums[mid] < target){

            first = mid + 1;

        }else{

            last = mid - 1;
        }
    }

    return first;
}