package binarysearch;

public class CeilingOfANumber {
    //Q: You are given an array (sorted) and a target number
    // so the ceiling number will be the smallest number in the array that is greater than or equal to the target number



    public static void main(String[] args) {

        int [] arr = {2,3,4,9,14,16,18};

        System.out.println(getCeilingNumber(arr, 18));

    }

    static int getCeilingNumber(int [] arr, int target){
        if(arr.length == 0) return -1;


        int first = 0;
        int last = arr.length - 1;
        while(first <= last){
            int mid = first + (last - first) / 2;

            if(arr[mid] == target){
                return mid;
            }
            if(arr[mid] < target){
                first = mid + 1;
            }else{
                last = mid - 1;
            }
        }

        //while the program execution reaches here the index or value of first will be greater than last because it breaks the while loop i.e while first is greater than last the while loop will break
        return first;
    }
}

