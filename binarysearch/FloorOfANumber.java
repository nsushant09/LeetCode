package binarysearch;

public class FloorOfANumber {
    public static void main(String[] args) {

        //floor = greatest number that is smaller or equal to the target number;

        int [] arr = {2,3,4,9,14,16,18};
        System.out.println(getFloorNumber(arr, 20));
    }

    static int getFloorNumber(int [] arr, int target){

        if(arr.length == 0){
            return -1;
        }
        int first = 0;
        int last = arr.length - 1;
        while(first <= last){
            int mid = first + (last - first) / 2;
            if(arr[mid] == target){
                return mid;
            }
            if(arr[mid] < target){
                first = mid + 1;
            }else{
                last = mid -1;
            }
        }

        return last;
    }
}
