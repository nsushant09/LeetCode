package binarysearch

fun main(){
    val list = listOf<Int>(1, 3, 5, 7, 9 , 100, 201, 304, 506 ,567, 678, 911)
    print(pseudoBinarycontains(7, list))
}

fun pseudoBinarycontains(value : Int, numbers : List<Int>) : Int{
    if(numbers.isEmpty()) return -1

    var firstIndex = 0
    var lastIndex = numbers.size - 1
    var middleIndex = (firstIndex + lastIndex) / 2

    while(firstIndex <= lastIndex){

        if(value == numbers[middleIndex]){
            return middleIndex
        }

        if(value < numbers[middleIndex]){
            lastIndex = middleIndex - 1
            middleIndex = (firstIndex + lastIndex) / 2
        }else{
            firstIndex = middleIndex + 1
            middleIndex = (firstIndex + lastIndex) / 2
        }

    }

    return -1
}