package binarysearch

fun main(){

    val nums = intArrayOf(99,99,80,75,22,11,10,5,2,-3)
    if(isAscending(nums)){
        println(searchBinaryAscending(nums, 22))
    }else{
        println(searchBinaryDescending(nums, -3))
    }
}

fun isAscending(nums : IntArray) : Boolean{
    return nums[0] <= nums[nums.size - 1]
}

fun searchBinaryAscending(nums : IntArray, target : Int) : Int{
    var first = 0;
    var last = nums.size - 1;
    var mid = -1;

    while(first <= last){
        mid = first + (last - first ) / 2;

        if(nums.get(mid) == target){
            return mid;
        }
        if(nums.get(mid) < target){
            first = mid + 1
        }else{
            last = mid - 1
        }
    }

    return -1;
}

fun searchBinaryDescending(nums : IntArray, target : Int) : Int{
    var first = 0;
    var last = nums.size - 1;
    var mid = -1;

    while(last >= first ){

        mid = first + (last - first) / 2

        if(nums.get(mid) == target){
            println("in mid")
            return mid ;
        }
        if(nums.get(mid) < target){

            last = mid - 1;
        }else{

            first = mid + 1;
        }

    }

    return -1;
}